==================================================
Learning Yampa and Functional Reactive Programming
==================================================

This blog post was originally written back in `2010-06-13 <https://web.archive.org/web/20100823084116/http://lambdor.net/?p=44>`_

If you are just starting to learn Yampa, please share your experiences! This post just covers my personal opinion on how to tackle Yampa, maybe you have additional or different recommendations.

Recommendations for learning
============================

I recommend reading the following papers/presentations to learn Yampa and FRP in general:

* `A Brief Introduction to Functional Reactive Programming and Yampa (slides) <https://web.archive.org/web/20100823084116/http://www.cs.nott.ac.uk/%7Enhn/FoPAD2007/Talks/nhn-FoPAD2007.pdf>`_
* `Arrows, FRP, and Functional Reactive Programming (PPT) <https://web.archive.org/web/20100823084116/http://plucky.cs.yale.edu/CS429F04/LectureSlides/YampaForCs429.ppt>`_
* `Arrows, Robots and Functional Programming <https://web.archive.org/web/20100823084116/http://www.haskell.org/yale/papers/oxford02/index.html>`_: covers Yampa basics in detail (Section 3 is very domain specific and may be omitted)
* `Functional Reactive Programming, Continued <https://web.archive.org/web/20100823084116/http://www.haskell.org/yale/papers/haskellworkshop02/index.html>`_: more Yampa basics
* `The Yampa Arcade <https://web.archive.org/web/20100823084116/http://www.haskell.org/yale/papers/haskell-workshop03/index.html>`_: standard paper for games
* `Dynamic, Interactive Virtual Environments <https://web.archive.org/web/20100823084116/http://imve.informatik.uni-hamburg.de/files/71-Blom-Diss-online.pdf>`_: read chapter 3 – Time and appendix A – Functional Reactive Programming
* `Functional Programming and 3D Games <https://web.archive.org/web/20100823084116/http://www.cse.unsw.edu.au/~pls/thesis/munc-thesis.pdf>`_: Yampa basics in games, not very detailed though
* `Functional Reactive Programming from First Principles <https://web.archive.org/web/20100823084116/http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.23.360>`_: Yampa implementation details
* `Dynamic Optimization for Functional Reactive Programming <https://web.archive.org/web/20100823084116/http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.102.7810>`_: Yampa optimization details

Understanding FRP
=================

I think to learn FRP (for games) you have to especially understand the following aspects:

* Signals make time omnipresent
* Systems are built with **Signal Functions** (``SF a b``)
* FRP is implemented in standard Haskell
* **Arrow notation** makes using FRP convenient and more readable
* Signal functions **diagrams** look just mirrored to the actual arrow notation code :)
* The signal function systems need to be updated somehow – usually via ``reactimate``
* ``reactimate`` divides the programm into input IO (sense), the signal function (``SF``) and output IO (actuate)
* Switches allow dynamic changes of the reactive system. Note that in Yampa signal functions are continuation-based so they "switch into" a new signal function.
* To handle dynamic game object collections use the **delayed parallel switch** (``dpSwitch``) signal function
* **Input events** are propagated to the objects via ``route``
* ``route`` also reasons about the whole object collection to produce **logical events** (f.e. hit detection)
* ``killOrSpawn`` collects all kill and spawn events into **one big function composition** (insertion/deletion) which is applied to the object collection
* In the Space Invaders example ``gameCore :: IL Object -> SF (GameInput, IL ObjOutput) (IL ObjOutput)`` is actually embedded in the function ``core :: ... -> SF GameInput (IL ObjOutput)`` which acts as the intermediate between sense and actuate (this is not mentioned in the Yampa Arcade paper)

Complete list of recommended papers
===================================

Covering FRP in general and FRP in games:

* `A Brief Introduction to Functional Reactive Programming and Yampa (slides) <https://web.archive.org/web/20100823084116/http://www.cs.nott.ac.uk/%7Enhn/FoPAD2007/Talks/nhn-FoPAD2007.pdf>`_
* `Arrows, FRP, and Functional Reactive Programming (PPT) <https://web.archive.org/web/20100823084116/http://plucky.cs.yale.edu/CS429F04/LectureSlides/YampaForCs429.ppt>`_
* `Arrows, Robots and Functional Programming <https://web.archive.org/web/20100823084116/http://www.haskell.org/yale/papers/oxford02/index.html>`_
* Directions in Functional Programming for RealTime Applications
* `Dynamic, Interactive Virtual Environments <https://web.archive.org/web/20100823084116/http://imve.informatik.uni-hamburg.de/files/71-Blom-Diss-online.pdf>`_
* `Dynamic Optimization for Functional Reactive Programming <https://web.archive.org/web/20100823084116/http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.102.7810>`_
* `Functional Programming and 3D Games <https://web.archive.org/web/20100823084116/http://www.cse.unsw.edu.au/~pls/thesis/munc-thesis.pdf>`_
* `Functional Reactive Programming from First Principles <https://web.archive.org/web/20100823084116/http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.23.360>`_
* `Functional Reactive Programming, Continued <https://web.archive.org/web/20100823084116/http://www.haskell.org/yale/papers/haskellworkshop02/index.html>`_
* Plugging A Space Leak With An Arrow
* Push-Pull Functional Reactive Programming
* `Push-Pull Functional Reactive Programming (video) <https://web.archive.org/web/20100823084116/http://vimeo.com/6686570>`_
* `The Yampa Arcade <https://web.archive.org/web/20100823084116/http://www.haskell.org/yale/papers/haskell-workshop03/index.html>`_

List of discarded papers
========================

The reason for discarding was mostly because they are too old, too theoretical or off-topic from games:

* A Functional Reactive Animation Of A Lift Using Fran
* A Language for Declarative Robotic Programming
* Crafting Game-Models Using Reactive System Design
* Event-Driven FRP
* FrTime – A Language for Reactive Programs
* Functional Reactive Animation
* `Functional Reactive Programming for Real-Time Reactive Systems <https://web.archive.org/web/20100823084116/http://haskell.cs.yale.edu/yale/papers/zwthesis/index.html>`_
* Genuinely Functional User Interfaces
* Interactive Functional Objects in Clean
* Modelling Reactive Multimedia – Events and Behaviours
* Modular Domain Specific Languages and Tools
* Prototyping Real-Time Vision Systems
* Reactive Multimedia Documents in a Functional Framework
* Real-Time FRP
