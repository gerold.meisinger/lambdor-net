{-# LANGUAGE Arrows #-}

import Paths_lambdacube_examples (getDataFileName)

import Control.Monad.Trans

import FRP.Yampa

import Graphics.UI.GLFW as GLFW
import Graphics.LambdaCube
import Graphics.LambdaCube.RenderSystem.GL
import qualified Graphics.LambdaCube.Loader.StbImage as Stb

w :: Int
w = 640

h :: Int
h = 480

type ObjInput  = (Int, Int)
type ObjOutput = (String, Proj4)

main :: IO ()
main = do
    mediaPath <- getDataFileName "media"
    renderSystem <- mkGLRenderSystem
    runLCM renderSystem [Stb.loadImage] (reactimate (initput title mediaPath) input output (process objs))
    closeWindow
  where
    title = "LambdaCube Engine Basic Example"
    objs  = [cam] -- if this is empty, it will immediately exit

initput :: RenderSystem r vb ib q t p lp
        => String -> String
        -> LCM   (World r vb ib q t p lp) e (Bool, ObjInput)
initput title mediaPath = do
    liftIO initialize
    liftIO $ openWindow defaultDisplayOptions
            { displayOptions_numRedBits   = 8
            , displayOptions_numGreenBits = 8
            , displayOptions_numBlueBits  = 8
            , displayOptions_numDepthBits = 24
            }
    liftIO $ setWindowTitle title

    addResourceLibrary [("General", [(PathDir, mediaPath)])]
    addScene
        [ node "Root" "Obj"    idmtx [mesh (Just RQP_EarlySky) Nothing "Box.mesh.xml"]
        , node "Root" "Light"  idmtx [defaultLight]
        , node "Root" "Cam"    idmtx [simpleCamera "Cam0"]
        ]
    addRenderWindow "MainWindow" w h [viewport 0 0 1 1 "Cam0" []]

    return (False, (0, 0))

input :: RenderSystem r vb ib q t p lp
      => Bool
      -> LCM   (World r vb ib q t p lp) e (DTime, Maybe (Bool, ObjInput))
input _ = liftIO $ do
    t <- getTime
    resetTime

    (mx, my) <- getMousePosition

    k <- keyIsPressed KeyEsc
    return (realToFrac t, Just (k, (mx, my)))

output :: RenderSystem r vb ib q t p lp
       => Bool -> [ObjOutput]
       -> LCM   (World r vb ib q t p lp) e Bool
output _ [] = return True
output _ os = do
    liftIO . print . show . head $ os
    updateTransforms os
    updateTargetSize "MainWindow" w h
    renderWorld    1 "MainWindow"

    liftIO swapBuffers
    return False

process :: [SF ObjInput ObjOutput] -> SF (Bool, ObjInput) [ObjOutput]
process sfs = proc (esc, mxy) -> do
    os <- parB sfs -< mxy
    returnA -< if esc then [] else os

cam :: SF ObjInput ObjOutput
cam = proc _ -> do
    t <- localTime -< ()
    returnA -< ("Cam", translation (Vec3 0 0 (realToFrac t)))
