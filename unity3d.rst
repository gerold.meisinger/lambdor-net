====================================================
Connecting Haskell FRP to the Unity3D game framework
====================================================

This blog post was originally written back in `2011-04-02 <https://web.archive.org/web/20120502034608/http://lambdor.net/?p=321>`_

.. image:: Unity-DllTest.png

Abstract
========

This post describes how to connect Haskell to Unity3D by compiling a Haskell shared library to a Win32 DLL and dynamically loading the DLL in a scripted Unity3D object via DLLImport. The DLL also uses an additional module (Yampa), thus FRP can be used within a sophisticated game framework (with the exception that this concept doesn't connecte to the Unity3D core yet, which would allow stateful arrows within the game logic... which renders it rather useless right now :/). Unity3D was chosen because it provides a complete game creation workflow and a demo version is available for free. In contrast, there are no full game creation frameworks available for Haskell right now (to my knowledge, but my status is based on ). DLLs were chosen over .NET libraries because to my knowledge it is not (yet) possible in Haskell to compile to a .NET library (and I think it was one of the reasons why F# was born). .NET libraries would of course allow for an easier integration in Unity3D. Unity plugins are only available in the pro version... which is a pity. My personal goal was to connect `functional temporal programming <http://conal.net/blog>`_ (functional reactive programming) to a sophisticated game framework to allow the creation of great games with FRP game logic. Besides, it was a great exercise for foreign function interfaces (FFI) and a test on how well Haskell can be integrated into existing applications.

Instructions
============

What actually happens when a Haskell program is packed into a shared library and called from an external program?
The actual DLL is built from C code, which exports the Haskell functions and also starts up a Haskell runtime, which delegates the external function calls to Haskell. All functions are called within an unsafe environment, like ``main :: IO ()`` (... this shouldn't necessarily be the case I think – FFI to Miranda?)

1. Install at least `GHC 7 <https://www.haskell.org>`_, because Windows DLLs don't work with GHC 6 (see: `GHC 7 change-log, section 1.5.4. DLLs: "Shared libraries are once again supported on Windows." <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/release-7-0-1.html>`_).

2. Download and install `Unity3D <https://www.unity3d.com>`_.

3. Download :download:`DllTest.hs` (Haskell code for game logic) and adopt to your needs.

  * As these function get evaluated within a unsafe environment, they are all IO Monads.
  * In Windows you have to use ``ccall`` (citation needed)

4. Download :download:`DllTest.cs` (Unity3D script file) and adopt to your needs.

  * Make sure to start and stop the Haskell runtime correctly. Nothing special really, just tag the external functions with ``[DllImport]`` and call.

5. Visit GHC manual and search for ``// StartEnd.c`` and save the code to ``StartEnd.c``.

  * This is the actual C program which is converted to a Windows DLL. It also starts up a Haskell runtime and allows the external program to call functions.

6. Install all dependent modules with shared library option enabled (f.e. yampa)

>>> cabal install --reinstall --enable-shared yampa.

7. Copy shared libraries of all used modules to your Haskell project directory ``C:/Program Files (x86)/Haskell Platform/2011.2.0.0/lib/yampa.../libHSYampa-0.9.2.3-ghc7.0.2.dll.a`` (...there may be a better solution for this)

8. Copy the file ``C:/Program Files (x86)/Internet Explorer/IEShims.dll`` to your Haskell project directory (... I forgot why)

9. Compile step 1

>>> ghc -dynamic -fPIC -c DllTest.hs

  * This is an intermediate step (see `Shared libraries that export a C API <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/using-shared-libs.html>`_) which produces the object file (.o) from C code (DllTest.o and DllTest_stub.o)
  * The ``-fPIC`` flag is required for all code that will end up in a shared library.

10. Compile step 2

>>> ghc -dynamic -shared DllTest.o DllTest_stub.o StartEnd.o libHSYampa-0.9.2.3-ghc7.0.2.dll.a -o libHsDllTest.dll

  * The object files, the runtime starter and the archive file for the additional module are compiled together into the final DLL.

11. Use the DependencyWalker tool of the Visual Studio IDE to find errors in the DLL (it may be included in `Visual Studio Express or Visual Studio Code <https://visualstudio.microsoft.com/>`_ which is available for free).

12. Create a Unity3D project and add an object using the ``DllTest.cs`` as a script file (this is out-of-scope of this article).

Notes on Haskell DLLs
---------------------

* If you didn't work with shared libraries before, make sure you understand the `Foreign function interface (FFI) <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/ffi.html>`_ and `all the different types of shared libraries <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/using-shared-libs.html>`_. What's used here is a **dynamically loaded shared library (DLL)** which gets loaded by an external (non-Haskell) programm (Unity3D).
* Read `GHC manual on Building and using Win32 DLLs <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/win32-dlls.html>`_ (Question: What is the option ``-lfooble`` good for?).
* All modules used by the Haskell program have to be compiled into **one-big DLL**. GHC 7 manual on Win32 DLLs: "Adder is the name of the root module in the module tree (as mentioned above, there must be a single root module, and hence a single module tree in the DLL".
* Make sure **shared-library versions** are installed of all used modules (see above). They get installed somewhere into `C:/Program Files (x86)/Haskell Platform/2011.2.0.0/lib...`
* The GHC 7 documentation is outdated (see: `GHC Ticket #4825: DLLs not documented consistently <http://hackage.haskell.org/trac/ghc/ticket/4825>`_).

Notes on Unity3D
----------------

* When the DLL file is recompiled, **Unity3D has to be restarted** in order to reload the DLL.
* If Unity3D doesn't find the DLL file, make sure the correct DLL file (in the directory where ``ghc`` is executed) is located in your Unity3D project where the .exe file is located which gets compiled by Unity3D.
* Unity3D log files are located in: ``C:/Users/myuser/AppData/Local/Unity``.
* Unity3D crash reports are located in: ``C:/Users/gerold/AppData/Local/Temp/crash_...``
* `stdcall instead of ccall <http://www.mail-archive.com/haskell-cafe@haskell.org/msg70066.html>`_ on Windows, this appears to be invalid for Unity3D (also see `GHC manual on Win32 DLLs <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/win32-dlls.html>`_.
* Unity3D uses the following script languages: C#, JavaScript, Boo.

Description
===========

The actual concept is pretty straight forward. Simply write all Haskell functions and make the function interfacing to the external program an IO monad, which gets called by a potentially inpure program. The program and all depending modules are compiled into one big DLL. This DLL gets imported by the external program (Unity3D) and makes the functions visible. Before an Haskell function can be called, a complete Haskell runtime environment has to be started. In the following example, when an object is created, it starts up a Haskell runtime, runs an embedded arrow and shuts the Haskell runtime down again. This is of course rather useless right now, but it shows an arrow running in Unity3D.

DllTest.hs (short)

.. code::

  {-# LANGUAGE ForeignFunctionInterface, Arrows #-}

  module Adder (
    embeddedSF,
    ...
  ) where

  import FRP.Yampa

  embeddedSF :: Double -> Double -> Double -> IO Double
  embeddedSF v0 v1 v2 = return . last $ embed integral (v0, [(1.0, Just v1), (1.0, Just v2)])

  foreign export ccall embeddedSF :: Double -> Double -> Double -> IO Double

DllTest.cs (short)

.. code:: csharp

  class DLLTest : MonoBehaviour {
  [DllImport ("libHSDllTest")]
  private static extern void HsStart();
  [DllImport ("libHSDLLTest")]
  private static extern void HsEnd();
  [DllImport ("libHSDLLTest")]
  private static extern double embeddedSF(double v0, double v1, double v2);

  void Awake () {
    HsStart();
    print ("embeddedSF: " + embeddedSF(12.0, 23.0, 45.0));
    HsEnd();
  }

Resources
=========

Haskell DLLs
------------

* `Foreign Declarations <https://web.archive.org/web/20110402/https://www.cse.unsw.edu.au/~chak/haskell/ffi/ffi/ffise3.html>`_
* `Stack overflow: Haskell compile dll <http://stackoverflow.com/questions/3228564/haskell-compile-dll>`_
* `GHC wiki – Using the FFI <http://www.haskell.org/haskellwiki/GHC/Using_the_FFI>`_
* `GHC manual – Chapter 8. Foreign function interface (FFI) <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/ffi.html>`_
* `GHC manual – 11.6. Building and using Win32 DLLs <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/win32-dlls.html>`_
* `GHC manual – 4.12. Using shared libraries <https://downloads.haskell.org/ghc/7.0-latest/docs/html/users_guide/using-shared-libs.html>`_
* `Real World Haskell – Chapter 17. Interfacing with C: the FFI <http://book.realworldhaskell.org/read/interfacing-with-c-the-ffi.html>`_
* `[Haskell-cafe] FFI, C/C++ and undefined references <http://www.mail-archive.com/haskell-cafe@haskell.org/msg69924.html>`_
* `Update: Creating a Windows DLL from a Haskell Program and calling it from C++ (todo) <http://blogs.msdn.com/b/satnam_singh/archive/2011/04/19/creating-a-windows-dll-from-a-haskell-program-and-calling-it-from-c.aspx>`_

Unity3D
-------

* `Documentation on Scripts <https://web.archive.org/web/20110402/http://www.unifycommunity.com/wiki/index.php?title=Scripts>`_
* `Programming Introduction <https://web.archive.org/web/20110402/http://www.unifycommunity.com/wiki/index.php?title=Programming_Introduction>`_
* `Extensions <http://https://web.archive.org/web/20110402/www.unifycommunity.com/wiki/index.php?title=Extensions>`_
* `Event Execution Order <http://https://web.archive.org/web/20110402/www.unifycommunity.com/wiki/index.php?title=Event_Execution_Order>`_
* `Scripting forum <http://forum.unity3d.com/forums/12-Scripting>`_
* "`Plugins <http://unity3d.com/support/documentation/Manual/Plugins.html>`_ are a Pro-only feature [probably developer side only]. For desktop builds, plugins will work in standalones only. They are disabled when building a Web Player for security reasons."
* `Using F# with Unity <http://answers.unity3d.com/questions/13196/using-f-with-unity.html>`_ "There are some things that are available to general .NET code that are not supported by Unity."
* `What would it take to make F# work with Unity? <http://answers.unity3d.com/questions/12037/what-would-it-take-to-make-f-work-with-unity.html>`_
* `Is it possible to build a standalone .net application that link UnityEngine.dll <http://answers.unity3d.com/questions/7579/is-it-possible-to-build-a-standalone-net-applicati.html>`_, `Is it possible to start Unity from a C# project? <http://answers.unity3d.com/questions/1284/is-it-possible-to-start-unity-from-a-c-project/1286>`_: "This is not possible. UnityEngine.dll is a small .NET wrapper around the native Unity engine. Without the Unity engine itself, UnityEngine.dll is merely an empty wrapper around nothing.There is no way to somehow get access to the Unity API in a homegrown .net application. The UnityEngine API is tightly coupled to its Unity host. They are one, and can't be split."

Open questions
==============

I don't know how to integrate **stateful arrows** into the game logic yet. Probably the Haskell runtime within the DLL has to be started on some OnGameStart-event. Each object has to run its own independent arrow code and the update ticks propagated to something like the ``reactimate`` procedure.
