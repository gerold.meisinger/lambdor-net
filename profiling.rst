====================================
Installing Haskell profile libraries
====================================

This blog post was originally written back in `2010-11-23 <https://web.archive.org/web/20131228190452/http://lambdor.net/?p=258>`_

`Profiling with GHC <https://web.archive.org/web/20131228190452/http://haskell.org/ghc/docs/6.10.2/html/users_guide/profiling.html>`_ is done by compiling with the ``-prof`` parameter, which compiles the program with profile code that can be activated by executing the program with ``./myprogram +RTS -p -RTS``. This creates log file called ``myprogram.prof``.

>>> ghc -prof --make -O2 myfile.hs
>>> ./myfile +RTS -p -RTS
>>> cat myfile.prof

Unfortunately this doesn't work out-of-the-box. You have to install the profiling versions of all libraries and all depending libraries. And even more unfortunately, `cabal doesn't resolve dependencies for profiling libraries <https://web.archive.org/web/20131228190452/http://hackage.haskell.org/trac/hackage/ticket/282>`_.

>>> cabal install --reinstall -p libraryname

**Update**: To automatically install the profiling libraries for **future installations** edit the ``~/.cabal/config`` and set the profiling options to ``True``. (thanks Erik)

**Update2**: You may only do this for libraries. I got ``leksah-server`` complaining about ``-N2`` option not being available with profiling executable.

But the reinstallation produces errors like:

.. code:: text

  Directory.hs:2:4:
      The export item `Permissions(Permissions, readable, writable,
                                   executable, searchable)'
      attempts to export constructors or class methods that are not visible here
  cabal: Error: some packages failed to install:
  MissingH-1.1.0.3 depends on haskell98-1.0.1.1 which failed to install.
  haskell98-1.0.1.1 failed during the building phase. The exception was:
  ExitFailure 1

or

.. code:: text

  >>     Could not find module `Data.List.Utils':
  >>       Perhaps you haven't installed the profiling libraries for package
  >> missingh-1.1.0.3?
  >>       Use -v to see a list of the files searched for.

I also tried removing and recompiling some packages completely like so

>>> ghc-pkg unregister MissingH    # case-sensitive
>>> cabal unpack MissingH
>>> cd MissingH-1.1.0.3/
>>> cabal configure -prof
>>> cabal build
>>> cabal install

There are some fundamental libraries which cannot be installed via cabal (like ``parsec3``), so you rather have to:

>>> sudo apt-get install libghc6-*-prof

But eventually I found out that my packages are broken anyway (check with ``ghc-pkg check``) and I had to reinstall Haskell platform, which also didn't work because:

>>> sudo apt-get purge ghc6    # didn't work

Just do:

>>> mv ~/.ghc6 ~/.ghc6.bak
>>> sudo apt-get purge ghc6
>>> sudo apt-get install --reinstall haskell-platform

Some links which were quite helpful:

* `haskell-cafe@haskell.org – Profiling <https://web.archive.org/web/20131228190452/http://comments.gmane.org/gmane.comp.lang.haskell.cafe/73669>`_
* `Stackoverflow – Mysterious cabal install problems <https://web.archive.org/web/20131228190452/http://hackage.haskell.org/trac/hackage/ticket/282>`_