===========================================
Running primitive signal functions in Yampa
===========================================

This blog post was originally written back in `2010-06-12 <https://web.archive.org/web/20100822210238/http://lambdor.net/?p=34>`_

To test signal functions in Yampa, use the ``embed`` function. Enter the following commands in the Haskell command-line to show the header definition:

>>> :type FRP.Yampa.embed
-- FRP.Yampa.embed :: FRP.Yampa.SF a b -> (a, [(FRP.Yampa.DTime, Maybe a)]) -> [b]

So the parameters are:

1. the signal function to run
2. a tuple of...

  1. the first input value at time=0
  2. and a list of... ``(time, Nothing|Just nextValue)``

and return a list of values produced by the signal function.

Primitive signal functions include: ``time``, ``identity`` and ``constant``

.. code::

  main :: IO ()
  main = do
      putStrLn $ show $ embed time (Nothing, [(1.0, Nothing), (0.2, Nothing), (0.03, Nothing)])
      putStrLn $ show $ embed time (123, [(1.0, Just 234), (0.2, Just 345), (0.03, Just 456)])
      -- [0.0,1.0,1.2,1.23]
      -- [0.0,1.0,1.2,1.23]

      putStrLn $ show $ embed identity (123, [(1.0, Just 234), (0.2, Just 345), (0.03, Just 456)])
      putStrLn $ show $ embed identity (537, [(1.0, Nothing), (0.2, Nothing), (0.03, Just 123)])
      -- [123,234,345,456]
      -- [537,537,537,537]

      putStrLn $ show $ embed (constant 537) (Nothing, [(1.0, Nothing), (0.2, Nothing), (0.03, Nothing)])
      putStrLn $ show $ embed (constant 537) (123, [(1.0, Just 234), (0.2, Just 345), (0.03, Just 456)])
      -- putStrLn $ show (embed constant (123, [(1.0, Just 234), (0.2, Just 345), (0.03, Just 456)])) -- ERROR
      -- [537,537,537,537]
      -- [537,537,537,537]
