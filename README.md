# Lambdor.net Devblog

This was a series of blog post articles written back in 2010-2012 about Haskell, Yampa and game development. The [original Wordpress blog lambdor.net](https://www.lambdor.net) ([archived](https://web.archive.org/web/20120626180055/http://lambdor.net)) eventually got corrupted, so some of the articles are revived here on GitLab and Read The Docs.

You can find the articles at https://lambdor-net.readthedocs.io
