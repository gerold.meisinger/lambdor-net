====================================
Activity diagram of Yampa reactimate
====================================

This blog post was originally written back in `2010-07-10 <https://web.archive.org/web/20100822074010/http://lambdor.net/?p=113>`_

.. image:: yampa_reactimate_activity.png

download: :download:`diagram (.svg) <yampa_reactimate_activity.svg>`, fonts (:download:`cmr10.ttf <http://mirrors.ctan.org/fonts/cm/ps-type1/bakoma/ttf/cmr10.ttf>`, :download:`cmtt10.ttf <http://mirrors.ctan.org/fonts/cm/ps-type1/bakoma/ttf/cmtt10.ttf>`)

1. Collect the input events (``init`` and ``input``) in an IO task.
2. Pass them to ``process`` (which is purely functional) and...
3. ``core`` is a ``Yampa.dpSwitch`` which consists of ``route``, the object list ``IL sf`` and ``killAndSpawn``.
4. ``route`` first reasons about all **previous object states** to produce logical events (collisions etc.) and...
5. secondly bundles the input and logical events to the objects (``IL (ObjEvents, sf)``).
6. Run all the signal functions which in turn may produce kill or spawn requests of new objects...
7. which are applied in ``killAndSpawn`` to possibly get a new object collection (``IL Object``) which are then fed-back into ``core``.
8. Render all objects states and loop.
