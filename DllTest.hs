-- Adder.hs
{-# LANGUAGE ForeignFunctionInterface, Arrows #-}

module Adder (
	adder,
	foo,
	bar,
	embeddedSF,
--	module FRP.Yampa,
	FRP.Yampa.VectorSpace -- already covered by FRP.Yampa
) where

import FRP.Yampa

adder :: Int -> Int -> IO Int  -- gratuitous use of IO
adder x y = return (x+y)

foreign export ccall adder :: Int -> Int -> IO Int

foo :: Int -> IO Int
foo d0 = return (1 + d0)

foreign export ccall foo :: Int -> IO Int

bar :: Double -> IO Double
bar d0 = return (1.0 + d0)

foreign export ccall bar :: Double -> IO Double

embeddedSF :: Double -> Double -> Double -> IO Double
embeddedSF v0 v1 v2 = return . last $ embed integral (v0, [(1.0, Just v1), (1.0, Just v2)])
--embeddedSF v0 v1 v2 = return . last $ embed (hold 0.0) ((Event v0), [(1.0, Just (Event v1)), (1.0, Just (Event v2))])

foreign export ccall embeddedSF :: Double -> Double -> Double -> IO Double
