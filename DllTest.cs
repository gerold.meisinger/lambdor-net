using UnityEngine;
using System.Runtime.InteropServices;

class DLLTest : MonoBehaviour {
	// This tells unity to look up the function FooPluginFunction
	// inside the plugin named "PluginName"
	[DllImport ("libHSDLLTest")]
	private static extern void HsStart();
	[DllImport ("libHSDLLTest")]
	private static extern void HsEnd();
	[DllImport ("libHSDLLTest")]
	private static extern int adder(int x, int y);
	[DllImport ("libHSDLLTest")]
	private static extern int foo(int d0);
	[DllImport ("libHSDLLTest")]
	private static extern double bar(double d0);
	[DllImport ("libHSDLLTest")]
	private static extern double embeddedSF(double v0, double v1, double v2);
	//private static extern float FooPluginFunction();

void Awake () {
	// Calls the FooPluginFunction inside the PluginName plugin
	// And prints 5 to the console
	HsStart();
	print ("adder" + adder(1, 2));	
	print ("foo" + foo(1));	
	print ("bar" + bar(1.2));	
	print ("embeddedSF: " + embeddedSF(12.0, 23.0, 45.0));	
	HsEnd();
	//print ("" + FooPluginFunction());
	}

void Update() {
	transform.Rotate(0, 5, 0);
	//print ("" + FooPluginFunction());
	//print("rotate");
	}
} 
